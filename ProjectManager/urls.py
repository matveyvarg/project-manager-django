"""ProjectManager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/dev/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings

from django.conf.urls import url,include
from django.contrib import admin
from base import views
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$',views.index,name='index'),
    url(r'^add_project/$',views.add_project,name='add'),
    url(r'^(?P<id>[0-9]+)$',views.retrieve,name='retrieve'),
    url(r'^add_task/$',views.add_task,name='add_task'),
    url(r'^switch_status/$',views.switch_status,name='switch'),
    url(r'^upload_file/$',views.upload_file,name='upload_file'),
    url(r'^change_project_fields/$',views.change_project_fields,name='change_project_fields'),
    url(r'^finished/$',views.finished,name='finished'),
    url(r'^file/(?:project_(?P<pid>[0-9]+)/(?P<fid>[0-9]+))/$',views.download_file,name='download_file'),
    url(r'^(?P<pid>[0-9]+)/delete',views.delete_project,name='delete_project'),
    url(r'^file/(?P<pid>[0-9]+)/(?P<fid>[0-9]+)/$',views.delete_file,name='delete_file'),
    url(r'^login/$',views.log_in,name='login'),
    url(r'^logout/$',views.log_out,name='logout'),
    url(r'^register/$',views.register,name='register'),
    url(r'^active/$',views.active,name='active'),
    url(r'^search/$',views.search,name='search'),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    #url(r'^froala_editor/', include('froala_editor.urls')),

] + static(settings.MEDIA_URL)
