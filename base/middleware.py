from django.http import HttpResponse, HttpResponsePermanentRedirect
from django.conf import settings
from django.shortcuts import redirect, resolve_url
# from django.core.urlresolvers import resolve

from django.utils.deprecation import MiddlewareMixin


class BasicAuthMiddleware(object):
    
    def __init__(self,get_response):
        self.get_response = get_response


    def __call__(self,request):
        
        response = self.get_response(request)

        return response