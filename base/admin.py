# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from base.models import *
# Register your models here.
class ProjectAdmin(admin.ModelAdmin):
	list_display = ['id','name','description','git_link','bitbucket_link',]
admin.site.register(Project,ProjectAdmin)

admin.site.register(File)