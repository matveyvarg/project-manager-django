# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,redirect,get_object_or_404
from django.contrib.auth import authenticate, login,logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User


from django.http import HttpResponse

from base.models import *

from .forms import *
# Create your views here.

# First Page function
@login_required
def index(request):
	projects = request.user.projects.all()

	return render(request,'base/index.html',{'projects':projects})

# Add form function
@login_required
def add_project(request):

	if request.method == 'POST':

		project_form = ProjectForm(request.POST)
		
		if project_form.is_valid():

			project = project_form.save()

			project.user = request.user
			project.save()
		
		else:
			print(project_form.errors)
	
		if len(request.FILES) > 0:

			filename = request.FILES['project_file'].name.split('/')[-1]

			new_file = File(filename = filename,
							project=project,
							project_file=request.FILES['project_file'],
							filetype=request.FILES['project_file'].content_type)
			new_file.save()
		
			
		return redirect('retrieve',id=project.id)


	else:
		project_form = ProjectForm()

		projs = request.user.projects.all()
		
	return render(request,'base/base.html',{'form':project_form,'proj':projs})
	
#display one project
@login_required
def retrieve(request,id):

	project = Project.objects.get(id=id)

	if(not request.user == project.user):
		redirect('index')
	files = File.objects.filter(project=project)

	
	tasks = project.get_sorted_tasks()

	rate = project.get_rate()
	
	return render(request,'base/project.html',{ 'project':project,
												'files':files,
												'tasks':tasks,
												'rate':rate})


@login_required
def add_task(request):

	if request.method == "POST":

		project = Project.objects.get(id=request.POST['project_id'])

		task = Task(project=project,name=request.POST['task_name'],status=True)

		task.save()

	return redirect('retrieve',id=request.POST['project_id'])


@login_required
def switch_status(request):

	if request.method == "POST":

		task = get_object_or_404(Task,id=request.POST['task_id'])

		task.status = not task.status

		task.save()

	return redirect('retrieve',id=task.project.id)  


@login_required
def upload_file(request):

	if request.method == "POST":

		project = get_object_or_404(Project,id=request.POST['project_id'])

		filename = request.FILES['project_file'].name.split('/')[-1]

		new_file = File(filename=filename,
						project=project,
						project_file=request.FILES['project_file'],
						filetype=request.FILES['project_file'].content_type)
		new_file.save()

	return redirect('retrieve',id=request.POST['project_id'])


@login_required
def change_project_fields(request):

	if request.method == "POST":

		if request.POST['type'] == 'description':

			project = get_object_or_404(Project,id=request.POST['project_id'])

			project.description = request.POST['description']

			project.save()


		elif request.POST['type'] == 'links':

			project = get_object_or_404(Project,id=request.POST['project_id'])

			project.git_link  = request.POST['github_link']

			project.bitbucket_link = request.POST['bitbucket_link']

			project.save()
	
	return redirect('retrieve',id = request.POST['project_id'])


@login_required
def finished(request):
	
	finish = []

	projects = request.user.projects.all()

	for project in projects:

		if project.get_rate() == 100:

			finish.append(project)

	return render(request,'base/index.html',{'projects':finish})


@login_required
def download_file(request,pid,fid):

	project = get_object_or_404(Project,id=pid)

	file = get_object_or_404(File,project=project,id=fid)
	
	response = HttpResponse(file.project_file,content_type=file.filetype)


	response['Content-Disposition'] ='attachment; filename=%s' %file.project_file.name

	return response 


@login_required
def delete_project(request,pid):

	project = get_object_or_404(Project,id = pid)

	project.delete()

	return redirect('index')


@login_required
def delete_file(request,pid,fid):

	project = get_object_or_404(Project,id=pid)

	file = get_object_or_404(File,project=project,id =fid)

	file.delete()

	return redirect('retrieve',id=pid)


def log_in(request):

	errors = []

	if request.method == "POST":

		username = request.POST['username']

		password = request.POST['password']

		user = authenticate(username=username, password=password)

		if user is not None:

			login(request, user)

			return redirect('index')
		else:
			errors.append('Invalid username or password')           

		return render(request,'base/login.html',{'errors':errors})

	elif request.method == "GET":

		return render(request,'base/login.html')


@login_required
def log_out(request):

	logout(request)

	return redirect('login')


def register(request):

	if request.method == "GET":

		return render(request,'base/register.html')

	elif request.method == "POST":

		username = request.POST['username']

		password = request.POST['password']

		email  = request.POST['email']

		user = User.objects.create(username=username,email=email,password=password)

		user.save()

		login(request,user)

		return redirect('index')

	else:
		return redirect('index')


@login_required
def active(request):

	active = []

	projects = Project.objects.all()

	for pj in projects:

		if pj.get_rate() != 100:
			active.append(pj)


	return render(request,'base/index.html',{'projects':active})


@login_required
def search(request):

	query = ''

	if len(request.GET) !=0:

		query = request.GET['search_field']

	projects = Project.objects.filter(name__contains=query)

	return render(request,'base/index.html',{'projects':projects})
	