# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.contrib.auth.models import User

import base64
# Create your models here.
from ckeditor.fields import RichTextField
from froala_editor.fields import FroalaField



# Set upload path 
def upload_path(instance,filename):
	
	
	ecs_str = filename.split('.')
	new_filename = base64.b64encode(ecs_str[-2])
	filetype = ecs_str[-1]
	#filetype = 
	return 'project_{0}/{1}.{2}'.format(instance.project.id,new_filename,filetype)

def default_user():

	return User.objects.get(username='matveyvarg').id
# Main Project class 
class Project(models.Model):

	user = models.ForeignKey(User,related_name='projects',default=default_user, on_delete=models.CASCADE)

	name = models.CharField(max_length=256)

	description = models.TextField(max_length=512)

	# Links for project
	git_link = models.URLField(max_length=1024,blank=True)
	bitbucket_link = models.URLField(max_length=1024,blank=True)
	

	def __unicode__(self):
		return self.name

	def get_rate(self):
		tasks = self.task.all()

		if len(tasks) != 0:

			done_tasks = tasks.filter(status=False)

			rate = (100/len(tasks))*len(done_tasks)

		else:

			rate = 0

		return rate


	def get_unfinished(self):

		unfinished = self.task.filter(status=True)

		return unfinished

	def get_sorted_tasks(self):

		sorted_tasks = []

		finished  = self.task.filter(status=False)

		unfinished = self.get_unfinished()

		sorted_tasks += finished

		sorted_tasks += unfinished

		return sorted_tasks

	def get_last_three(self):

		tasks = self.get_unfinished()

		last_three = []

		if len(tasks) > 3:

			for i in range(0,3):

				last_three.append(tasks[i])

		else:

			last_three = tasks

		return last_three



# Image Model for projects
class File(models.Model):

	#filename 
	filename = models.CharField(default='new_file',max_length=1024)

	# key to link Project Model
	project = models.ForeignKey('Project',related_name="files",  on_delete=models.CASCADE)

	# project Images
	project_file = models.FileField(upload_to=upload_path,null=True)

	filetype = models.CharField(max_length=32,default='none')
	#project_image = models.ImageField(upload_to='/uploads/',blank=True, null = True)


	def __unicode__(self):
		return self.filename

class Task(models.Model):

	project = models.ForeignKey('Project',related_name="task",  on_delete=models.CASCADE)

	name = models.CharField(max_length=64)

	status = models.BooleanField(default=True)



