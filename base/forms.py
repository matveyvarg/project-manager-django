from django import forms
from .models import *

class FileForm(forms.ModelForm):

	class Meta:
		model = File

		fields = ('project_file',)

class ProjectForm(forms.ModelForm):

	
	class Meta: 
		model = Project

		fields = ('name', 'description','git_link','bitbucket_link')